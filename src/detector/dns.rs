extern crate hex;

pub fn detect(packet_data: &[u8]) -> bool {
    let data = &packet_data.get(44..46);
    match data {
        Some(data) => {
            let port = hex::encode(data);
            port.contains("0100") || port.contains("8180")
        }
        None => false,
    }
}
