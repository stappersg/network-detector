extern crate hex;

pub mod decoder;
pub mod detector;

use pcap::Device;
use pcap_file::pcap::PcapReader;
use std::fs::File;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "network-detector")]
enum Opt {
    /// analyse pcap file
    Pcap {
        /// Set pcap file path to parse file input, else capture method is active
        #[structopt(name = "FILE")]
        file: String,
    },
    /// analyse live traffic
    Capture,
}

fn capture_interface() {
    let mut cap = Device::lookup()
        .unwrap()
        .open()
        .expect("you need root permission to capture packets");

    while let Ok(packet) = cap.next() {
        if detector::dns::detect(&packet.data) {
            let packet_info: decoder::packet::Packet =
                decoder::packet::get_packet_information(&packet.data);
            println!("{:#}", packet_info);
            decoder::dns::decode(&packet.data);
        }
    }
}

fn read_pcap_file(pcap_file_path: &String) {
    let file_in = File::open(pcap_file_path).expect("Error opening file");
    let pcap_reader = PcapReader::new(file_in).unwrap();

    for pcap in pcap_reader {
        let pcap = pcap.unwrap();
        if detector::dns::detect(&pcap.data) {
            let packet: decoder::packet::Packet =
                decoder::packet::get_packet_information(&pcap.data);
            println!("{:#}", packet);
            decoder::dns::decode(&pcap.data);
        }
    }
}

fn main() {
    let opt = Opt::from_args();

    match &opt {
        Opt::Capture => capture_interface(),
        Opt::Pcap { file } => read_pcap_file(&file),
    }
}
