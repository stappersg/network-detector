use std::fmt;

pub struct L3 {
    destination: String,
    source: String,
}

impl fmt::Display for L3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "src: {}, dst: {}", self.source, self.destination)
    }
}

pub fn extract_data(packet_data: &[u8]) -> L3 {
    let dest_nums: Vec<String> = packet_data[26..30].iter().map(|n| n.to_string()).collect();
    let src_nums: Vec<String> = packet_data[30..34].iter().map(|n| n.to_string()).collect();

    L3 {
        destination: dest_nums.join("."),
        source: src_nums.join("."),
    }
}
