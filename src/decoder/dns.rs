extern crate hex;

use std::fmt;
use std::str;

struct Dns {
    request: String,
    domain_name: String,
}

impl fmt::Display for Dns {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "type: {} -> domain name: {}",
            self.request, self.domain_name
        )
    }
}

pub fn decode(packet_data: &[u8]) {
    let domain_name: String = parse_query_domain_name(&packet_data);
    let dns_type = match hex::encode(&packet_data.get(44..46).unwrap()).as_ref() {
        "0100" => String::from("query"),
        "8180" => String::from("response"),
        _ => String::from(""),
    };

    let dns = Dns {
        request: dns_type,
        domain_name,
    };

    println!("{:#}", dns);
}

fn parse_query_domain_name(packet_data: &[u8]) -> String {
    let mut domain_name = String::from("");
    let mut index: usize = 54;

    loop {
        let char_counter = packet_data[index] as usize;
        if char_counter == 0 {
            break;
        }
        index = index + 1;
        if domain_name.len() > 0 {
            domain_name.push_str(".");
        }
        domain_name.push_str(
            str::from_utf8(&packet_data.get(index..index + char_counter).unwrap()).unwrap(),
        );

        index = index + char_counter;
    }
    domain_name
}

#[allow(dead_code)]
fn get_index_of_dns_section(packet_data: &[u8]) -> usize {
    let mut index: usize = 54;
    loop {
        let char_counter = packet_data[index] as usize;
        if char_counter == 0 {
            break;
        }
        index += 1;
    }
    index + 5
}
