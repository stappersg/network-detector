# Network Detector

`network-detector` is my first app which is written in rust. It should analyze
live traffic and pcap files. The first detector should detect the DNS protocol
(port based).

For each detected DNS protocol the app should extract packet information.

### Example

```bash
cargo build
./target/debug/network-detector capture
```

or

```bash
cargo build
./target/debug/network-detector pcap pcaps/example.pcap
```

demo output (f35e955b121241b9582023c296312e2db4dff6b9)

```bash
Ether: src: 00:00:00:12:34:56, dst: AA:AA:AA:12:34:56, IP: src: 10.10.0.1, dst: 10.10.0.122
found dns (40)
Ether: src: AA:AA:AA:12:34:56, dst: 00:00:00:12:34:56, IP: src: 10.10.0.122, dst: 10.10.0.1
found dns (41)
Ether: src: AA:AA:AA:12:34:56, dst: 00:00:00:12:34:56, IP: src: 10.10.0.122, dst: 10.10.0.1
found dns (42)
```
